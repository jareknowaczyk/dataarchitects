# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class BiOrgunitemployeerel(models.Model):
    employee_id = models.IntegerField()
    orgunit_id = models.IntegerField(db_column='orgUnit_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'bi_orgunitemployeerel'


class BiUnitorg(models.Model):
    name = models.CharField(max_length=150)
    description = models.TextField()
    adr_street = models.CharField(max_length=150)
    adr_city = models.CharField(max_length=150)
    adr_post_code = models.CharField(max_length=30)
    creation_date = models.DateField()
    country = models.ForeignKey('SapCountry', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'bi_unitorg'


class BiUserunitrel(models.Model):
    org_unit = models.ForeignKey(BiUnitorg, models.DO_NOTHING)
    user = models.ForeignKey('LinxUsers', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'bi_userunitrel'


class CostCenterCodes(models.Model):
    cc_id = models.AutoField(primary_key=True)
    cc_sap_code = models.CharField(max_length=10)
    cc_name = models.CharField(max_length=50, blank=True, null=True)
    record_valid_from = models.DateField(blank=True, null=True)
    record_valid_to = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cost_center_codes'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class ExplorerQuery(models.Model):
    title = models.CharField(max_length=255)
    sql = models.TextField()
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    last_run_date = models.DateTimeField()
    created_by_user = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)
    snapshot = models.BooleanField()
    connection = models.CharField(max_length=128, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'explorer_query'


class ExplorerQuerylog(models.Model):
    sql = models.TextField(blank=True, null=True)
    run_at = models.DateTimeField()
    query = models.ForeignKey(ExplorerQuery, models.DO_NOTHING, blank=True, null=True)
    run_by_user = models.ForeignKey(AuthUser, models.DO_NOTHING, blank=True, null=True)
    duration = models.FloatField(blank=True, null=True)
    connection = models.CharField(max_length=128, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'explorer_querylog'


class LinxUsers(models.Model):
    user_id = models.AutoField(primary_key=True)
    linx_login = models.CharField(max_length=20)
    name_1 = models.CharField(max_length=60, blank=True, null=True)
    surname_1 = models.CharField(max_length=60, blank=True, null=True)
    boss_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'linx_users'


class SapCountry(models.Model):
    country_id = models.AutoField(primary_key=True)
    coundy_code = models.CharField(max_length=10, blank=True, null=True)
    contry_name = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sap_country'
