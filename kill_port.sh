#!/usr/bin/env bash

terminate(){
  lsof -P | grep ':$1' | awk '{print $2}' | xargs kill -9
}

terminate