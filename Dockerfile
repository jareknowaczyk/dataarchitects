 FROM python:3.6
 ENV PYTHONUNBUFFERED 1
 RUN mkdir /Insighter360
 WORKDIR /Insighter360
 ADD requirements.txt /Insighter360/
 RUN pip install -r requirements.txt
 ADD . /Insighter360/