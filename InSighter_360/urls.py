"""mdd_bi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.urls import path
from django.contrib import admin
from bi import views
from django.contrib.auth import views as auth_views
from django.contrib.auth import logout
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))



urlpatterns = [
    url(r'^login/$',
               auth_views.LoginView.as_view(template_name= os.path.join(BASE_DIR, 'bi/templates/registration/login.html')),
                   name='login'),
    url(r'^logout/$', views.logout_view),
    url(r'access_denied/', views.access_denied),
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.index),
    # url(r'^real-time-customer-360/customer-360-degree-view/',views.customer_360_view),
    # # path(r'watchlist/<int:pk>/', views.watchlist_detail, name='watchlist_detail'),
    # url(r'watchlist/new/', views.watchlist_new, name='watchlist_new'),
    path(r'products/', views.product_list, name='product_list'),
    path(r'products/detail', views.product_detail, name='product_detail'),
 
]