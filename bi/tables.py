import django_tables2 as tables
from .models import Report

from models import Report
from table import Table
from table.columns import Column

class ReportTable(Table):
    name = Column(field='id')
    description = Column(field='name')
    class Meta:
        model = Report