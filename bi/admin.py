from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from bi.models import Product, DimProducts


class ProductAdmin(admin.ModelAdmin):
    list_display = ('product_name', 'product_code', 'product_cost', 'product_category')

admin.site.register(Product, ProductAdmin)
admin.site.register(DimProducts)
