# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _




class UserProductRel(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(DAProduct,on_delete=models.CASCADE)

    def __str__(self):
        return "Relation: " +self.user +" - "+ self.product


class Product(models.Model):
    product_name = models.CharField(max_length = 100)
    product_code = models.CharField(max_length = 100)
    product_cost = models.IntegerField(default = 0)
    product_category = models.CharField(max_length = 100)
    product_brand = models.CharField(max_length = 100)
    product_subcategory = models.CharField(max_length = 100, blank = True)
    product_isdeleted = models.BooleanField()
    def add(self):
        self.save()

    def __str__(self):
        return self.product_name


# class WatchList(models.Model):
#     user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
#     brand = models.CharField(max_length = 100, default ='test')
#     model = models.CharField(max_length = 100, default ='test')
#     tyreWidth = models.IntegerField(default = 0)
#     tyreHeight = models.IntegerField(default = 0)
#     tyreRimDiameter = models.IntegerField(default = 0)
#     tyreConstructionType = models.IntegerField(default = 0)
#     speedIndex = models.IntegerField(default = 0)
#     tyreLoadIndex = models.IntegerField(default = 0)
#     created_date = models.DateTimeField(default=timezone.now)
    
#     def add(self):
#         self.save()

#     def __str__(self):
#         return self.model