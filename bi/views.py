from django.shortcuts import render
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required,permission_required
from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth.models import Group
from django.http import JsonResponse
from django.utils import timezone
from .forms import productSubmitForm

import requests
import pandas as pd
import simplejson as json
from collections import namedtuple

@login_required(login_url='/login/')
def index(request):

	return render(request, 'index.html')


@login_required(login_url='/login/')
def access_denied(request):
	return render(request, '401.html')


def logout_view(request):
	logout(request)
	return redirect('/')	

def product_list(request):
	form = productSubmitForm(request.POST or None, user=request.user)
	products = requests.get('http://ec2-54-93-207-27.eu-central-1.compute.amazonaws.com:8080/product/manager/get/product/all').text
	products = json.loads(products, object_hook=lambda d: namedtuple('products', d.keys())(*d.values()))
	if request.method == 'POST' and form.is_valid():
		response = requests.post('http://ec2-54-93-207-27.eu-central-1.compute.amazonaws.com:8080/product/manager/add/product', json = {"brand":  form.cleaned_data['brand'],
																																	 	"customerProductId":  form.user.pk,
																																		 "customerUrl": form.cleaned_data['url'],
																																		 "description": form.cleaned_data['description'],
																																		 "manufacturerName": form.cleaned_data['manufacturer'],
																																		 "price": form.cleaned_data['price'],
																																		 "productName": form.cleaned_data['name'],
																																	 	 "productEan": form.cleaned_data['ean'],
																																		 })
		response = response.text
		products = requests.get('http://ec2-54-93-207-27.eu-central-1.compute.amazonaws.com:8080/product/manager/get/product/all').text
		products = json.loads(products, object_hook=lambda d: namedtuple('products', d.keys())(*d.values()))
	return render(request, 'product-table.html', {'products':products, 'form': form})


def product_detail(request):
	return render(request, 'product_detail.html')

