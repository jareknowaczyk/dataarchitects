# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django_measurement.models import MeasurementField
from measurement.measures import Weight

class DaCustomersCompetitors(models.Model):
    dacustomer_id = models.BigIntegerField()
    competitors_id = models.BigIntegerField(unique=True)

    class Meta:
        managed = False
        db_table = 'da_customers_competitors'


class DaCustomersProducts(models.Model):
    dacustomer_id = models.BigIntegerField()
    products_id = models.BigIntegerField(unique=True)

    class Meta:
        managed = False
        db_table = 'da_customers_products'


class DimAdress(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    city = models.CharField(max_length=255, blank=True, null=True)
    country = models.CharField(max_length=255, blank=True, null=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    geomx = models.DecimalField(max_digits=19, decimal_places=2, blank=True, null=True)
    geomy = models.DecimalField(max_digits=19, decimal_places=2, blank=True, null=True)
    post_code = models.CharField(max_length=255, blank=True, null=True)
    street = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dim_adress'


class DimCstCompetitors(models.Model):
    id = models.BigIntegerField(primary_key=True)
    business_units = models.BinaryField(blank=True, null=True)
    cooperated_companies = models.BinaryField(blank=True, null=True)
    created_date_time = models.DateTimeField(auto_now_add=True)
    customer_orientation = models.BinaryField(blank=True, null=True)
    description = models.BinaryField(blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)
    income = models.BinaryField(blank=True, null=True)
    market_share = models.BinaryField(blank=True, null=True)
    marketing_campaign_types = models.BinaryField(blank=True, null=True)
    markets = models.BinaryField(blank=True, null=True)
    modified_date_time = models.DateTimeField(blank=True, null=True)
    number_of_employees = models.BinaryField(blank=True, null=True)
    offline_market_share = models.BinaryField(blank=True, null=True)
    online_market_share = models.BinaryField(blank=True, null=True)
    related_companies = models.BinaryField(blank=True, null=True)
    revenue = models.BinaryField(blank=True, null=True)
    sales_channels = models.BinaryField(blank=True, null=True)
    social_media = models.BinaryField(blank=True, null=True)
    strategic_goals = models.BinaryField(blank=True, null=True)
    tax_code = models.CharField(max_length=255, blank=True, null=True)
    web_site = models.CharField(max_length=255, blank=True, null=True)
    adress = models.ForeignKey(DimAdress, models.DO_NOTHING, blank=True, null=True)
    is_active = models.BooleanField()
    record_valid_to = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dim_cst_competitors'


class DimCstCompetitorsBunits(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    business_unit_type = models.IntegerField()
    business_units = models.BinaryField(blank=True, null=True)
    cooperated_companies = models.BinaryField(blank=True, null=True)
    created_date_time = models.DateTimeField(auto_now_add=True)
    customer_orientation = models.BinaryField(blank=True, null=True)
    description = models.BinaryField(blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)
    income = models.BinaryField(blank=True, null=True)
    market_share = models.BinaryField(blank=True, null=True)
    marketing_campaign_types = models.BinaryField(blank=True, null=True)
    markets = models.BinaryField(blank=True, null=True)
    modified_date_time = models.DateTimeField(auto_now=True)
    number_of_employees = models.BinaryField(blank=True, null=True)
    offline_market_share = models.BinaryField(blank=True, null=True)
    online_market_share = models.BinaryField(blank=True, null=True)
    related_companies = models.BinaryField(blank=True, null=True)
    revenue = models.BinaryField(blank=True, null=True)
    sales_channels = models.BinaryField(blank=True, null=True)
    social_media = models.BinaryField(blank=True, null=True)
    strategic_goals = models.BinaryField(blank=True, null=True)
    tax_code = models.CharField(max_length=255, blank=True, null=True)
    web_site = models.CharField(max_length=255, blank=True, null=True)
    is_active = models.BooleanField()
    record_valid_to = models.DateTimeField(blank=True, null=True)
    adress = models.ForeignKey(DimAdress, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dim_cst_competitors_bunits'


class DimCstCompetitorsBusinessUnits(models.Model):
    competitor = models.ForeignKey(DimCstCompetitors, models.DO_NOTHING)
    business_units = models.ForeignKey(DimCstCompetitorsBunits, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dim_cst_competitors_business_units'


class DimCstCompetitorsSocialMedia(models.Model):
    competitor = models.ForeignKey(DimCstCompetitors, models.DO_NOTHING)
    social_media = models.ForeignKey('DimSocialMedia', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dim_cst_competitors_social_media'


class DimDaCustomers(models.Model):
    id = models.BigIntegerField(primary_key=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)
    official_company_name = models.CharField(max_length=255, blank=True, null=True)
    short_name = models.CharField(max_length=255, blank=True, null=True)
    tax_code = models.CharField(max_length=255, blank=True, null=True)
    adress = models.ForeignKey(DimAdress, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dim_da_customers'


class DimDaCustomersCompetitors(models.Model):
    dacustomer = models.ForeignKey(DimDaCustomers, models.DO_NOTHING)
    competitors = models.ForeignKey(DimCstCompetitors, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dim_da_customers_competitors'


class DimDaCustomersProducts(models.Model):
    dacustomer = models.ForeignKey(DimDaCustomers, models.DO_NOTHING)
    products = models.ForeignKey('DimProducts', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dim_da_customers_products'


class DimProdCategoryDict(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    category_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dim_prod_category_dict'


class DimProdCategoryDictSubCategories(models.Model):
    product_category = models.ForeignKey(DimProdCategoryDict, models.DO_NOTHING, related_name='parent_category')
    sub_categories = models.ForeignKey(DimProdCategoryDict, models.DO_NOTHING, related_name='child_category')

    class Meta:
        managed = False
        db_table = 'dim_prod_category_dict_sub_categories'


class DimProdPageAttrs(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    is_valid = models.BooleanField(blank=True, null=True)
    name = models.CharField(max_length=255)
    selector = models.CharField(max_length=255, blank=True, null=True)
    valid_from = models.DateTimeField(blank=True, null=True)
    valid_to = models.DateTimeField(blank=True, null=True)
    value = models.CharField(max_length=255, blank=True, null=True)
    xpath = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dim_prod_page_attrs'


class DimProdPages(models.Model):
    id = models.BigIntegerField(primary_key=True)
    created_date_time = models.DateTimeField(auto_now_add=True)
    modified_date_time = models.DateTimeField(auto_now=True)
    competitor = models.ForeignKey(DimCstCompetitors, models.DO_NOTHING)
    competitor_bunit = models.ForeignKey(DimCstCompetitorsBunits, models.DO_NOTHING, blank=True, null=True)
    name = models.CharField(max_length=255)
    social_media_type = models.IntegerField()
    url = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'dim_prod_pages'


class DimProdPagesAttributes(models.Model):
    product_page = models.ForeignKey(DimProdPages, models.DO_NOTHING)
    attributes = models.ForeignKey(DimProdPageAttrs, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dim_prod_pages_attributes'


class DimProducts(models.Model):
    id = models.BigIntegerField(primary_key=True)
    brand = models.CharField(max_length=255, blank=True, null=True)
    crawl_schedule_intervalh = models.IntegerField()
    created_date_time = models.DateTimeField(auto_now_add=True)
    modified_date_time = models.DateTimeField(auto_now=True)
    customer_product_number = models.CharField(max_length=255, blank=True, null=True)
    customer_url = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255)
    is_available = models.BooleanField(blank=True, null=True, default=True)
    is_deleted = models.BooleanField(blank=True, null=True, default=True)
    manufacturer_name = models.CharField(max_length=255, blank=True, null=True)
    price = models.DecimalField(max_digits=19, decimal_places=2, blank=True, null=True)
    product_color = models.CharField(max_length=255, blank=True, null=True) 
    product_ean = models.CharField(max_length=255, blank=True, null=True)
    purchase_price = models.DecimalField(max_digits=19, decimal_places=2, blank=True, null=True)
    short_name = models.CharField(max_length=255, blank=True, null=True)
    sizex = models.DecimalField(max_digits=19, decimal_places=2, blank=True, null=True)
    sizey = models.DecimalField(max_digits=19, decimal_places=2, blank=True, null=True)
    sizez = models.DecimalField(max_digits=19, decimal_places=2, blank=True, null=True)
    stock = models.DecimalField(max_digits=19, decimal_places=2, blank=True, null=True)
    total_cost = models.DecimalField(max_digits=19, decimal_places=2, blank=True, null=True)
    valid_from = models.DateTimeField(blank=True, null=True)
    valid_to = models.DateTimeField(blank=True, null=True)
    volume = models.DecimalField(max_digits=19, decimal_places=2, blank=True, null=True)
    weight = MeasurementField(measurement=Weight)
    product_category = models.ForeignKey(DimProdCategoryDict, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dim_products'


class DimProductsProductPages(models.Model):
    product = models.ForeignKey(DimProducts, models.DO_NOTHING)
    product_pages = models.ForeignKey(DimProdPages, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'dim_products_product_pages'


class DimSocialMedia(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    created_date_time = models.DateTimeField(auto_now_add=True)
    modified_date_time = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=255)
    social_media_type = models.IntegerField()
    url = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'dim_social_media'


class Product(models.Model):
    product_name = models.CharField(max_length = 100)
    product_code = models.CharField(max_length = 100, blank = True)
    product_cost = models.IntegerField(default = 0, blank = True)
    product_category = models.CharField(max_length = 100, blank = True)
    product_brand = models.CharField(max_length = 100, blank = True)
    product_subcategory = models.CharField(max_length = 100, blank = True)
    product_isdeleted = models.BooleanField(default = False)
    def add(self):
        self.save()

    def __str__(self):
        return self.product_name
