
from django import forms


class productSubmitForm(forms.Form):
        brand = forms.CharField(max_length=100, label = "Product Brand")
        name = forms.CharField(max_length=100, label = "Product Name", required = False)
        ean = forms.DecimalField(required=True, label = "Product EAN")
        url = forms.URLField(max_length = 400, label ="Product URL",  required = False)
        description = forms.CharField(max_length=400, label = "Product Description",  required = False)
        manufacturer = forms.CharField(max_length=100, label = "Product Manufacturer",  required = False)
        price = forms.DecimalField(decimal_places=4, max_digits=19, label = "Product Price",  required = False)

        def __init__(self, *args, **kwargs):
                self.user = kwargs.pop('user', None)
                super(productSubmitForm, self).__init__(*args, **kwargs)

        



